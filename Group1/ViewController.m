//
//  ViewController.m
//  Group1
//
//  Created by click labs 115 on 10/16/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSMutableArray *arrayForAllCity;
    NSMutableArray *arrayForVisitedCity;
    NSMutableArray *ArrayForNonVisitedCity;
    NSMutableDictionary *dictForAllCity;
    UITableViewCell *cell;
    BOOL allowsMultipleSelectionDuringEditing;
}
@property (strong, nonatomic) IBOutlet UIButton *btnVisited;
@property (strong, nonatomic) IBOutlet UIButton *btnNonVisited;
@property (strong, nonatomic) IBOutlet UITableView *tblCity;

@end

@implementation ViewController
@synthesize tblCity;

- (void)viewDidLoad {
    [super viewDidLoad];
    ArrayForNonVisitedCity = [NSMutableArray new];
    tblCity.allowsMultipleSelectionDuringEditing = YES;

    _btnVisited.layer.borderWidth = 1.0f;
    _btnVisited.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnNonVisited.layer.borderWidth = 1.0f;
    _btnNonVisited.layer.borderColor = [UIColor whiteColor].CGColor;
    dictForAllCity = [NSMutableDictionary dictionary];
    [dictForAllCity setObject: @"Chandigarh"  forKey: @"100001"];
    [dictForAllCity setObject: @"Panchkula"   forKey: @"100002"];
    [dictForAllCity setObject: @"Mohali"      forKey: @"100003"];
    [dictForAllCity setObject: @"Ambhala"     forKey: @"100004"];
    [dictForAllCity setObject: @"Shimla"      forKey: @"100005"];
    [dictForAllCity setObject: @"Mansuri"     forKey: @"100006"];
    [dictForAllCity setObject: @"Jaipur"      forKey: @"100007"];
    [dictForAllCity setObject: @"Solan"       forKey: @"100008"];
    [dictForAllCity setObject: @"Kasoli"      forKey: @"100009"];
    [dictForAllCity setObject: @"Palampur"    forKey: @"100010"];
    [dictForAllCity setObject: @"Jodhpur"     forKey: @"100011"];
    [dictForAllCity setObject: @"Raipur"      forKey: @"100012"];
    [dictForAllCity setObject: @"Ajmer"       forKey: @"100013"];
    [dictForAllCity setObject: @"Patiala"     forKey: @"100014"];
    [dictForAllCity setObject: @"Hamirpur"    forKey: @"100015"];
    [dictForAllCity setObject: @"Sikar"       forKey: @"100016"];
    [dictForAllCity setObject: @"Ganganagar"  forKey: @"100017"];
    [dictForAllCity setObject: @"Bikaner"     forKey: @"100018"];
    [dictForAllCity setObject: @"Udaipur"     forKey: @"100019"];
    [dictForAllCity setObject: @"Ramghard"    forKey: @"100020"];
    
    
    arrayForAllCity = [NSMutableArray new];
    
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100001"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100002"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100003"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100004"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100005"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100006"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100007"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100008"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100009"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100010"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100011"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100012"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100013"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100014"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100015"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100016"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100017"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100018"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100019"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100020"]]];
    
    arrayForVisitedCity = [NSMutableArray new];
    //arrayForVisitedCity = [NSMutableArray arrayWithArray:arrayForAllCity];

    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayForAllCity count];
    
    
}

- (IBAction)showAllVisitedCity:(id)sender {
    
    arrayForAllCity = [NSMutableArray new];
    
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100001"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100002"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100003"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100004"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100005"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100006"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100007"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100008"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100009"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100010"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100011"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100012"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100013"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100014"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100015"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100016"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100017"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100018"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100019"]]];
    [arrayForAllCity addObject:[NSString stringWithFormat:@"%@",[dictForAllCity objectForKey:@"100020"]]];

    [tblCity reloadData];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" ];

    cell.textLabel.text =arrayForAllCity[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:@"unnamed.png"];
    //cell.imageView.image = [UIImage imageNamed:countryImage[indexPath.row]];
      //cell.textLabel.text = serchTableData [indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row %2) {
        [cell setBackgroundColor:[UIColor lightGrayColor]] ;
        
    }
    else{
        
        [cell setBackgroundColor:[UIColor grayColor]] ;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    //[tblCity reloadData];
    cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell.imageView setImage:[UIImage imageNamed:@"1.png"]] ;
    
    
    //[tblCity reloadData];
    [ArrayForNonVisitedCity addObject:[NSString stringWithFormat:@"%@",cell.textLabel.text]];
    
}
- (IBAction)deleteSelectedCity:(id)sender {
   
    for(int i = 0;i<[arrayForAllCity count];i++)
    {
        for(int j= 0;j<[ArrayForNonVisitedCity count];j++)
        {
            if([[arrayForAllCity objectAtIndex:i] isEqualToString:[ArrayForNonVisitedCity objectAtIndex:j]])
            {
                [arrayForAllCity removeObjectAtIndex:i];
            
            }
           [tblCity reloadData]; 
        }
        
    }
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
